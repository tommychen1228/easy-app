package com.myideaway.easyapp.sample.view;

import android.widget.Button;
import com.myideaway.easyapp.sample.R;
import com.myideaway.easyapp.sample.view.common.BaseActivity;
import com.myideaway.easyapp.sample.view.common.TipMessageBar;

public class MyActivity extends BaseActivity {

    @Override
    protected void onCreateMainView() {
        setMainView(R.layout.main);
        showNavigationBar(false);
        showToolBar(false);

        Button backButton = new Button(context);
        backButton.setText("Back");
        navigationBar.addLeftView(backButton);

        showTipMessageAndHide("Hello", TipMessageBar.TIP_ICON_TYPE_PROGRESS, 3000);
    }

}
