package com.myideaway.easyapp.sample.view;

import android.app.Application;
import com.myideaway.easyapp.sample.modal.common.Config;
import com.myideaway.easyapp.sample.view.common.ImageLoaderFactory;
import com.myideaway.easyapp.util.LogUtil;

/**
 * Created with IntelliJ IDEA.
 * User: duanchang
 * Date: 13-11-26
 * Time: 下午5:30
 * To change this template use File | Settings | File Templates.
 */
public class EasyApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        LogUtil.setLogLevel(LogUtil.LOG_ALL);
        Config.init(getApplicationContext());
        ImageLoaderFactory.init(getApplicationContext());
    }


}






