package com.myideaway.easyapp.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created with IntelliJ IDEA.
 * User: cdm
 * Date: 2/5/13
 * Time: 3:28 PM
 */
public class ResUtil {

    public static int getViewId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "id", context.getPackageName());
        return id;
    }

    public static int getDrawableId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return id;
    }

    public static int getArrayId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "array", context.getPackageName());
        return id;
    }

    public static int getColorId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "color", context.getPackageName());
        return id;
    }

    public static int getStringId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "string", context.getPackageName());
        return id;
    }

    public static int getAnimationId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "anim", context.getPackageName());
        return id;
    }

    public static int getAnimatorId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "animator", context.getPackageName());
        return id;
    }


    public static int getStyleId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "style", context.getPackageName());
        return id;
    }

    public static int getLayoutId(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "layout", context.getPackageName());
        return id;
    }

    public static int getRawId(Context context,String name){
        int id = context.getResources().getIdentifier(name, "raw", context.getPackageName());
        return id;
    }

    public static View getView(Activity activity, String name) {
        int id = getViewId(activity, name);
        View view = activity.findViewById(id);
        return view;
    }

    public static View getView(View parent, String name) {
        int id = getViewId(parent.getContext(), name);
        View view = parent.findViewById(id);
        return view;
    }

    public static String getString(Context context, String name) {
        int id = getStringId(context, name);
        String str = context.getResources().getString(id);
        return str;
    }

    public static Drawable getDrawable(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        Drawable drawable = context.getResources().getDrawable(id);
        return drawable;
    }

    public static Animation getAnimation(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "anim", context.getPackageName());
        Animation animation = AnimationUtils.loadAnimation(context, id);
        return animation;
    }

}
