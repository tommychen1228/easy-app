package com.myideaway.easyapp.util;

import android.util.Log;

public class LogUtil {

    public static final int LOG_OFF = -1;
    public static final int LOG_INFO = 1;
    public static final int LOG_DEBUG = 2;
    public static final int LOG_ERROR = 3;
    public static final int LOG_ALL = 4;

    private static String tag;
    private static int logLevel = LOG_INFO;

    public static void debug(String msg) {
        String appendedMsg = appendMsgAndInfo(msg, getCurrentInfo());
        if (logLevel >= LOG_DEBUG) {
            Log.d(tag, appendedMsg);
        }

    }

    public static void error(String msg, Throwable tr) {
        String appendedMsg = appendMsgAndInfo(msg, getCurrentInfo());
        if (logLevel >= LOG_ERROR) {
            Log.e(tag, appendedMsg, tr);
        }
    }

    public static void info(String msg) {
        String appendedMsg = appendMsgAndInfo(msg, getCurrentInfo());
        if (logLevel >= LOG_INFO) {
            Log.i(tag, appendedMsg);
        }
    }

    private static String getCurrentInfo() {

        StackTraceElement[] eles = Thread.currentThread().getStackTrace();
        StackTraceElement targetEle = eles[5];
        String info = "(" + targetEle.getClassName() + "."
                + targetEle.getMethodName() + ":" + targetEle.getLineNumber()
                + ")";
        return info;
    }

    private static String appendMsgAndInfo(String msg, String info) {
        return msg + " " + getCurrentInfo();
    }

    public static String getTag() {
        return tag;
    }

    public static void setTag(String tag) {
        LogUtil.tag = tag;
    }

    public static int getLogLevel() {
        return logLevel;
    }

    public static void setLogLevel(int logLevel) {
        LogUtil.logLevel = logLevel;
    }
}
