//
//  EasyAppCommonTests.m
//  EasyAppCommonTests
//
//  Created by cdm on 03/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "EasyAppCommonTests.h"
#import "EACCommon.h"
#import "ASIFormDataRequest.h"

@implementation EasyAppCommonTests

- (void)setUp {
    [super setUp];

    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.

    [super tearDown];
}

- (void)testLogDebug {
    EAC_LOG_D(self, @"testLogDebug");
}

- (void)testLogError {
    NSException *ex = [[NSException alloc] initWithName:@"TestException" reason:@"Test the exception" userInfo:nil];

    EAC_LOG_E(self, @"testLogError", ex);
}

- (void)testHTTPRequest {

    // param加密

    NSURL *requestUrl = [NSURL URLWithString:@"http://localhost:8080/egov/userLogin.htm"];
    // post request
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestUrl];
    request.requestMethod = @"POST";
    [request setPostValue:@"0102" forKey:@"userCode"];
    [request setPostValue:@"1" forKey:@"password"];

    [request startSynchronous];

    // result
    NSString *resultString = request.responseString;

    EAC_LOG_D(self, resultString);
}

@end
