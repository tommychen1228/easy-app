//
// Created by cdm on 12-6-28.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "EACService.h"

#define REQUEST_METHOD_GET @"GET"
#define REQUEST_METHOD_POST @"POST"

@interface EACRemoteServiceFile : NSObject

@property(nonatomic, retain) NSString *filePath;
@property(nonatomic, retain) NSString *paramName;

@end

@interface EACRemoteService : EACService {
@protected
    NSString *_targetURL;
    NSMutableDictionary *_sendParams;
    NSMutableArray *_sendFiles;
    NSString *_requestMethod;
    NSMutableDictionary *_requestHeaders;
}

@property(nonatomic, retain) NSString *targetURL;
@property(nonatomic, retain) NSMutableDictionary *sendParams;
@property(nonatomic, retain) NSMutableArray *sendFiles;
@property(nonatomic, retain) NSString *requestMethod;
@property(nonatomic, retain) NSMutableDictionary *requestHeaders;

- (NSString *)getURL;

- (void)addParams;

@end