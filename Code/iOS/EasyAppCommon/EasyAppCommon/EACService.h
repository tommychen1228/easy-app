//
//  Service.h
//  HuaShang
//
//  Created by cdm on 11-10-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>




@class EACService;

@protocol MIWWServiceDelegate <NSObject>

@required
- (id)service:(EACService *)service didSuccess:(id)result;

- (id)service:(EACService *)service didFault:(NSException *)exception;

@end

@interface EACService : NSObject

@property(nonatomic, retain) id delegate;
@property(nonatomic, assign) SEL onSuccessSelector;
@property(nonatomic, assign) SEL onFaultSelector;
@property(nonatomic, retain) id executeResult;
@property(nonatomic, retain) NSException *executeException;
@property(nonatomic, assign) void (^onSuccessBlock)(EACService *, id);
@property(nonatomic, assign) void (^onFaultBlock)(EACService *, id);

- (id)syncExecute;

- (void)asyncExecute;
- (void)asyncExecuteSuccess:(void (^)(EACService *, id))onSuccess andFault:(void (^)(EACService *, NSException *))onFault;

- (void)willExecute;

- (void)doSyncExecute;

- (void)doAsyncExecute;

- (id)didExecute:(id)result;

- (id)onExecute;

- (void)cancel;

@end
