//
// Created by cdm on 12-6-28.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "EACRemoteService.h"

@implementation EACRemoteServiceFile


@synthesize filePath = _filePath;
@synthesize paramName = _paramName;

- (void)dealloc {
    [_filePath release];
    [_paramName release];
    [super dealloc];
}


- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];

    [description appendString:@">"];
    [description appendFormat:@" filePath: %@, paramName: %@", _filePath, _paramName];
    return description;
}

@end

@implementation EACRemoteService {

}

@synthesize targetURL = _targetURL;
@synthesize sendParams = _sendParams;
@synthesize requestMethod = _requestMethod;
@synthesize requestHeaders = _requestHeaders;


@synthesize sendFiles = _sendFiles;

- (id)init {
    self = [super init];
    if (self) {
        [_sendParams release];
        _sendParams = [[NSMutableDictionary alloc] init];

        [_requestHeaders release];
        _requestHeaders = [[NSMutableDictionary alloc] init];

        [_sendFiles release];
        _sendFiles = [[NSMutableArray alloc] init];

        self.requestMethod = REQUEST_METHOD_POST;
    }

    return self;
}


- (void)willExecute {
    [super willExecute];

    self.targetURL = [[self getURL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self addParams];
}


- (NSString *)getURL {
    return nil;
}

- (void)addParams {

}

- (void)dealloc {
    [_targetURL release];
    [_sendParams release];
    [_requestMethod release];
    [_requestHeaders release];
    [_sendFiles release];
    [super dealloc];
}



@end